# docker-scipystack

A docker container with all the tools needed to run your own scipystack

## Versions

|Package|Version|
|:------|:-----:|
|`tensorflow`|`0.8.0`|
